package ru.edu;

import org.junit.Test;
import ru.edu.model.Registry;

public class RegistryExternalStorageJSONTest {

    public static final String OUTPUT_ARTIST_BY_COUNTRY_JSON = "./inputTest/artist_by_country.json";
    public static final String STORAGE_JSON = "./outputFromStorageJson/output.";
    public static final String BINARY = "serialized";
    public static final String JSON = "json";
    public static final String XML = "xml";

    RegistryExternalStorageJSON registryExternalStorageJson;
    RegistryExternalStorageXML registryExternalStorageXML;
    RegistryExternalStorageBinary registryExternalStorageBinary;
    Registry registry;

    @Test
    public void test() throws Exception {
        registryExternalStorageJson = new RegistryExternalStorageJSON();
        registryExternalStorageBinary = new RegistryExternalStorageBinary();
        registryExternalStorageXML = new RegistryExternalStorageXML();

        registryExternalStorageJson.readFrom(OUTPUT_ARTIST_BY_COUNTRY_JSON);
        registry = registryExternalStorageJson.getRegistry();

        registryExternalStorageBinary.writeTo(STORAGE_JSON + BINARY, registry);
        registryExternalStorageJson.writeTo(STORAGE_JSON + JSON, registry);
        registryExternalStorageXML.writeTo(STORAGE_JSON + XML, registry);
    }
}