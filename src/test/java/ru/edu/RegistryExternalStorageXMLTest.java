package ru.edu;

import org.junit.Test;
import ru.edu.model.Registry;

import static org.junit.Assert.*;

public class RegistryExternalStorageXMLTest {

    public static final String OUTPUT_ARTIST_BY_COUNTRY_XML = "./inputTest/artist_by_country.xml";
    public static final String STORAGE_XML = "./outputFromStorageXml/output.";
    public static final String BINARY = "serialized";
    public static final String JSON = "json";
    public static final String XML = "xml";

    RegistryExternalStorageJSON registryExternalStorageJson;
    RegistryExternalStorageXML registryExternalStorageXML;
    RegistryExternalStorageBinary registryExternalStorageBinary;
    Registry registry;

    @Test
    public void test() throws Exception {
        registryExternalStorageJson = new RegistryExternalStorageJSON();
        registryExternalStorageBinary = new RegistryExternalStorageBinary();
        registryExternalStorageXML = new RegistryExternalStorageXML();

        registryExternalStorageXML.readFrom(OUTPUT_ARTIST_BY_COUNTRY_XML);
        registry = registryExternalStorageXML.getRegistry();

        registryExternalStorageBinary.writeTo(STORAGE_XML + BINARY, registry);
        registryExternalStorageJson.writeTo(STORAGE_XML + JSON, registry);
        registryExternalStorageXML.writeTo(STORAGE_XML + XML, registry);
    }
}