package ru.edu.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.junit.Test;
import ru.edu.MapperUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ArtistTest {
    public static final String ARTIST_JSON = "./src/test/resources/example_files/artist/artist.json";
    public static final String ARTIST_XML = "./src/test/resources/example_files/artist/artist.xml";

    Artist artist;

    @Test
    public void jsonMapper() throws Exception {
        artist = MapperUtils.readJson(ARTIST_JSON, Artist.class);

        String jsonObject = new ObjectMapper().writeValueAsString(artist);

        assertNotNull(artist);
        assertEquals("{\"name\":\"ArtistName\",\"albums\":[]}", jsonObject);

        assertEquals("ArtistName", artist.getName());
    }

    @Test
    public void xmlMapper() throws Exception {
        artist = MapperUtils.readXml(ARTIST_XML, Artist.class);

        String xmlObject = new XmlMapper().writeValueAsString(artist);

        assertNotNull(artist);
        assertEquals("<Artist><Name>Example</Name><Albums/></Artist>", xmlObject);

        assertEquals("Example", artist.getName());
    }

//    @Test
//    public void constructorTest() {
//        List<Album> albumsTMP = new ArrayList<>();
//        Artist artistTest = new Artist("DIMA", albumsTMP);
//
//        assertEquals("DIMA", artistTest.getName());
//    }
}