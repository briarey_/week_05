package ru.edu.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.junit.Test;
import ru.edu.MapperUtils;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AlbumTest {
    public static final String ALBUM_JSON = "./src/test/resources/example_files/album/album.json";
    public static final String ALBUM_XML = "./src/test/resources/example_files/album/album.xml";

    Album album;

    @Test
    public void jsonMapper() throws Exception {
        album = MapperUtils.readJson(ALBUM_JSON, Album.class);

        String jsonObject = new ObjectMapper().writeValueAsString(album);

        assertNotNull(album);
        assertEquals("{\"name\":\"ololo\",\"year\":1993,\"price\":10.9}", jsonObject);

        assertEquals("ololo", album.getName());
        assertEquals(1993, album.getYear());
        assertEquals(1090, (int) (album.getPrice() * 100));
    }

    @Test
    public void xmlMapper() throws Exception {
        album = MapperUtils.readXml(ALBUM_XML, Album.class);

        String xmlObject = new XmlMapper().writeValueAsString(album);

        assertNotNull(album);
        assertEquals("<Album name=\"ExampleName\" year=\"1993\" price=\"10.9\"/>", xmlObject);

        assertEquals("ExampleName", album.getName());
        assertEquals(1993, album.getYear());
        assertEquals(1090, (int) (album.getPrice() * 100));
    }
}