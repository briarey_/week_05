package ru.edu.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.junit.Test;
import ru.edu.MapperUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class RegistryTest {
    public static final String REGISTER_JSON = "./src/test/resources/example_files/register/Register.json";
    public static final String REGISTER_XML = "./src/test/resources/example_files/register/register.xml";

    Registry registry;

    @Test
    public void jsomMapper() throws Exception {
        registry = MapperUtils.readJson(REGISTER_JSON, Registry.class);

        String jsonObject = new ObjectMapper().writeValueAsString(registry);

        assertNotNull(registry);
        assertEquals("{\"countries\":[],\"countryCount\":3}", jsonObject);

        assertEquals(3, registry.getCountryCount());
    }

    @Test
    public void xmlMapper() throws Exception {
        registry = MapperUtils.readXml(REGISTER_XML, Registry.class);

        String xmlObject = new XmlMapper().writeValueAsString(registry);

        assertNotNull(registry);
        assertEquals("<ArtistRegistry countryCount=\"3\"><Country/></ArtistRegistry>", xmlObject);

        assertEquals(3, registry.getCountryCount());
    }

//    @Test
//    public void constructorTest() {
//        List<Country> countriesTMP = new ArrayList<>();
//        Registry registryTest = new Registry(countriesTMP, 3);
//
//        assertEquals(3, registryTest.getCountryCount());
//    }
}