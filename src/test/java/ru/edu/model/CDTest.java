package ru.edu.model;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.junit.Test;
import ru.edu.MapperUtils;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CDTest {
    public static final String CD_XML = "./src/test/resources/example_files/cd.xml";

    CD cd;

    @Test
    public void xmlMapperTestThrowMapperUtils() throws Exception {
        cd = MapperUtils.readXml(CD_XML, CD.class);

        String xmlObject = new XmlMapper().writeValueAsString(cd);

        assertNotNull(cd);
        assertEquals("<CD><TITLE>For the good times</TITLE><ARTIST>Kenny Rogers</ARTIST><COUNTRY>UK</COUNTRY><COMPANY>Mucik Master</COMPANY><PRICE>8.7</PRICE><YEAR>1995</YEAR></CD>", xmlObject);

        assertEquals("For the good times", cd.getTitle());
        assertEquals("Kenny Rogers", cd.getArtist());
        assertEquals("UK", cd.getCountry());
        assertEquals("Mucik Master", cd.getCompany());
        assertEquals((int) (8.70 * 100), (int) (cd.getPrice() * 100));
        assertEquals(1995, cd.getYear());
    }

    @Test
    public void constructorTest() {
        CD cdTest = new CD("Title",
                "Artist",
                "Country",
                "Company",
                12.20,
                1993);

        assertEquals("Title", cdTest.getTitle());
        assertEquals("Artist", cdTest.getArtist());
        assertEquals("Country", cdTest.getCountry());
        assertEquals("Company", cdTest.getCompany());
        assertEquals(1220, (int) (cdTest.getPrice() * 100));
        assertEquals(1993, cdTest.getYear());
    }
}