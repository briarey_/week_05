package ru.edu.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.junit.Test;
import ru.edu.MapperUtils;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CountryTest {
    public static final String COUNTRY_JSON = "./src/test/resources/example_files/country/country.json";
    public static final String COUNTRY_XML = "./src/test/resources/example_files/country/country.xml";

    Country country;

    @Test
    public void jsonMapper() throws Exception {
        country = MapperUtils.readJson(COUNTRY_JSON, Country.class);

        String jsonObject = new ObjectMapper().writeValueAsString(country);

        assertNotNull(country);
        assertEquals("{\"name\":\"USA\",\"artists\":[]}", jsonObject);

        assertEquals("USA", country.getName());
    }

    @Test
    public void XmlMapper() throws Exception {
        country = MapperUtils.readXml(COUNTRY_XML, Country.class);

        String xmlObject = new XmlMapper().writeValueAsString(country);

        assertNotNull(country);
        assertEquals("<Country name=\"USA\"><Artist><Name/><Albums/></Artist></Country>", xmlObject);

        assertEquals("USA", country.getName());
    }
}