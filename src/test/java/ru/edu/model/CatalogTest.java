package ru.edu.model;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.junit.Test;
import ru.edu.CatalogParser;
import ru.edu.MapperUtils;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;

public class CatalogTest {
    public static final String INPUT_CD_CATALOG_XML = "./input/cd_catalog.xml";
    public static final String OUTPUT_ARTIST_BY_COUNTRY_XML = "./output/artist_by_country.xml";
    public static final String OUTPUT_ARTIST_BY_COUNTRY_JSON = "./output/artist_by_country.json";
    public static final String OUTPUT_ARTIST_BY_COUNTRY_SERIALIZED = "./output/artist_by_country.serialized";

    Catalog catalog;

    @Test
    public void xmlMapper() throws Exception {
        catalog = MapperUtils.readXml(INPUT_CD_CATALOG_XML, Catalog.class);
        assertNotNull(catalog);

        CatalogParser catalogParser = new CatalogParser(catalog);

        Registry registry = catalogParser.parser();
        MapperUtils.mapXml(OUTPUT_ARTIST_BY_COUNTRY_XML, registry);
        MapperUtils.mapJson(OUTPUT_ARTIST_BY_COUNTRY_JSON, registry);
        MapperUtils.mapSerialize(OUTPUT_ARTIST_BY_COUNTRY_SERIALIZED, registry);

    }

    /**
     * Такой же маппер, только через try catch с ресурсами.
     */
    @Test
    public void xmlMapper1() {
        XmlMapper mapper = new XmlMapper();

        try (FileInputStream file = new FileInputStream(CatalogTest.INPUT_CD_CATALOG_XML)) {
            catalog = mapper.readValue(file, Catalog.class);
            assertNotNull(catalog);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//
//    @Test
//    public void constructorTest() {
//        List<CD> cdTest = new ArrayList<>();
//        Catalog catalogTest = new Catalog(cdTest);
//    }
}