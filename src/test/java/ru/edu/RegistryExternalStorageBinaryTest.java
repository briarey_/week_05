package ru.edu;

import org.junit.Test;
import ru.edu.model.Registry;

public class RegistryExternalStorageBinaryTest {

    public static final String OUTPUT_ARTIST_BY_COUNTRY_SERIALIZED = "./inputTest/artist_by_country.serialized";
    public static final String STORAGE_BINARY = "./outputFromStorageBinary/output.";
    public static final String BINARY = "serialized";
    public static final String JSON = "json";
    public static final String XML = "xml";

    RegistryExternalStorageJSON registryExternalStorageJson;
    RegistryExternalStorageXML registryExternalStorageXML;
    RegistryExternalStorageBinary registryExternalStorageBinary;
    Registry registry;

    @Test
    public void test() throws Exception {
        registryExternalStorageJson = new RegistryExternalStorageJSON();
        registryExternalStorageBinary = new RegistryExternalStorageBinary();
        registryExternalStorageXML = new RegistryExternalStorageXML();

        registryExternalStorageBinary.readFrom(OUTPUT_ARTIST_BY_COUNTRY_SERIALIZED);
        registry = registryExternalStorageBinary.getRegistry();

        registryExternalStorageBinary.writeTo(STORAGE_BINARY + BINARY, registry);
        registryExternalStorageJson.writeTo(STORAGE_BINARY + JSON, registry);
        registryExternalStorageXML.writeTo(STORAGE_BINARY + XML, registry);
    }
}