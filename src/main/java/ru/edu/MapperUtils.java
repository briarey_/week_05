package ru.edu;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Короткая запись всех использующихся
 * серриализаторов и дессериализаторов.
 */
public final class MapperUtils {

    /**
     * Конструктор по умолчанию.
     */
    private MapperUtils() {
    }

    /**
     * Считывания Json файла.
     *
     * @param fileName
     * @param obj
     * @throws IOException
     */
    public static void mapJson(final String fileName, final Object obj)
            throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(new File(fileName), obj);
    }

    /**
     * Запись Json файла.
     *
     * @param fileName
     * @param tClass
     * @param <T>
     * @return Объект
     * @throws IOException
     */
    public static <T> T readJson(final String fileName, final Class<T> tClass)
            throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(new File(fileName), tClass);
    }

    /**
     * Считывание Xml файла.
     *
     * @param fileName
     * @param obj
     * @throws IOException
     */
    public static void mapXml(final String fileName, final Object obj)
            throws IOException {
        XmlMapper mapper = new XmlMapper();
        mapper.writeValue(new File(fileName), obj);
    }

    /**
     * Запись Xml файла.
     *
     * @param fileName
     * @param tClass
     * @param <T>
     * @return Объект
     * @throws IOException
     */
    public static <T> T readXml(final String fileName, final Class<T> tClass)
            throws IOException {
        XmlMapper mapper = new XmlMapper();
        return mapper.readValue(new File(fileName), tClass);
    }

    /**
     * Дессириализация бинарного файла.
     *
     * @param fileName
     * @param obj
     * @throws Exception
     */
    public static void mapSerialize(final String fileName, final Object obj)
            throws Exception {
        ObjectOutputStream outputStream =
                new ObjectOutputStream(new FileOutputStream(fileName));
        outputStream.writeObject(obj);
    }

    /**
     * Серриализация в бинарный файл.
     *
     * @param fileName
     * @param tClass
     * @param <T>
     * @return Объект
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static <T> T readSerialize(final String fileName,
                                      final Class<T> tClass)
            throws Exception {
        ObjectInputStream inputStream =
                new ObjectInputStream(new FileInputStream(fileName));
        return ((T) (inputStream.readObject()));
    }
}
