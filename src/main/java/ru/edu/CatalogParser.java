package ru.edu;

import ru.edu.model.Album;
import ru.edu.model.Artist;
import ru.edu.model.CD;
import ru.edu.model.Catalog;
import ru.edu.model.Country;
import ru.edu.model.Registry;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CatalogParser {

    /**
     * Каталог.
     */
    private final Catalog catalog;

    /**
     * Registry object variable.
     */
    private final Registry registry;

    /**
     * Constructor.
     *
     * @param cat Catalog object
     */
    public CatalogParser(final Catalog cat) {
        this.catalog = cat;
        this.registry = new Registry();
    }


    /**
     * Метод распределения данных из каталога.
     *
     * @return объект регистрации
     */
    public Registry parser() {
        Stream<CD> stream = catalog.getCdList().stream();


        Map<String, List<CD>> groupByCountryMap =
                stream.collect(Collectors.groupingBy(CD::getCountry));

        groupByCountryMap.forEach((countryName, countryList) -> {
            Country country = new Country(countryName);

            Map<String, List<CD>> groupByArtistMap
                    = countryList
                    .stream()
                    .collect(Collectors.groupingBy(CD::getArtist));

            groupByArtistMap.forEach((artistName, artistList) -> {
                Artist artist = new Artist(artistName);
                artistList.forEach(cd ->
                        artist.addAlbum(new Album(cd.getTitle(),
                                cd.getYear(), cd.getPrice())));
                country.addArtist(artist);
            });
            registry.addCountry(country);
        });
        return registry;
    }
}
