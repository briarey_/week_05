package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.Serializable;

@JacksonXmlRootElement(localName = "Album")
public class Album implements Serializable {

    /**
     * Название альбома.
     */
    @JacksonXmlProperty(localName = "name", isAttribute = true)
    @JsonProperty("name")
    private String name;

    /**
     * Год публикации.
     */
    @JacksonXmlProperty(localName = "year", isAttribute = true)
    @JsonProperty("year")
    private int year;

    /**
     * Цена альбома.
     */
    @JacksonXmlProperty(localName = "price", isAttribute = true)
    @JsonProperty("price")
    private double price;

    /**
     * Конструктор альбома с указанием названия, года публикации и цены.
     *
     * @param albumName
     * @param albumYear
     * @param albumPrice
     */
    public Album(final String albumName,
                 final int albumYear,
                 final double albumPrice) {
        this.name = albumName;
        this.year = albumYear;
        this.price = albumPrice;
    }

    /**
     * Конструктор по умолчанию.
     */
    public Album() {
    }

    /**
     * Получить название альбома.
     *
     * @return название
     */
    public String getName() {
        return name;
    }

    /**
     * Получить год публикации альбома.
     *
     * @return год
     */
    public long getYear() {
        return year;
    }

    /**
     * Получить цену альбома.
     * @return цена
     */
    public double getPrice() {
        return price;
    }
}
