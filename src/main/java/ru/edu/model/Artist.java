package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JacksonXmlRootElement(localName = "Artist")
public class Artist implements Serializable {

    /**
     * Псевдоним артиста.
     */
    @JacksonXmlProperty(localName = "Name")
    @JsonProperty("name")
    private String name;

    /**
     * Альбомы артиста.
     */
    @JacksonXmlElementWrapper(localName = "Albums")
    @JacksonXmlProperty(localName = "Album")
    @JsonProperty("albums")
    private List<Album> albums = new ArrayList<>();

    /**
     * Конструктор с псевдонимом и альбомами.
     *
     * @param artistName
     * @param artistAlbums
     */
    public Artist(final String artistName,
                  final List<Album> artistAlbums) {
        this.name = artistName;
        this.albums = artistAlbums;
    }

    /**
     * Конструктор с параметрами.
     *
     * @param pName
     */
    public Artist(final String pName) {
        this.name = pName;
    }

    /**
     * Конструктор по умолчанию.
     */
    public Artist() {
    }

    /**
     * Получить псевдоним артиста.
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Получить список альбомов.
     *
     * @return album
     */
    public List<Album> getAlbums() {
        return albums;
    }

    /**
     * Добавление альбома артисту.
     *
     * @param album Album object
     */
    public void addAlbum(final Album album) {
        albums.add(album);
    }
}
