package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JacksonXmlRootElement(localName = "ArtistRegistry")
public class Registry implements Serializable {

    /**
     * Список стран.
     */
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "Country")
    @JsonProperty("countries")
    private List<Country> countries = new ArrayList<>();

    /**
     * Количество стран.
     */
    @JacksonXmlProperty(localName = "countryCount", isAttribute = true)
    @JsonProperty("countryCount")
    private int countryCount = 0;

    /**
     * Конструктор со списком стран.
     *
     * @param pCountries
     * @param pCountryCount
     */
    public Registry(final List<Country> pCountries, final int pCountryCount) {
        this.countries = pCountries;
        this.countryCount = pCountryCount;
    }

    /**
     * Конструктор по умолчанию.
     */
    public Registry() {
    }

    /**
     * Получить список стран.
     *
     * @return список стран
     */
    public List<Country> getCountries() {
        return countries;
    }

    /**
     * Получить количество стран.
     *
     * @return количество стран
     */
    public int getCountryCount() {
        return countryCount;
    }

    /**
     * Добавить страну в реестр.
     *
     * @param country
     */
    public void addCountry(final Country country) {
        this.countries.add(country);
        countryCount++;
    }
}
