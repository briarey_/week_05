package ru.edu.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс каталог.
 */
@JacksonXmlRootElement(localName = "CATALOG")
public class Catalog {

    /**
     * Список дисков.
     */
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "CD")
    private List<CD> cdList = new ArrayList<>();

    /**
     * Конструктор с параметрами.
     *
     * @param catalogCdList
     */
    public Catalog(final List<CD> catalogCdList) {
        this.cdList = catalogCdList;
    }

    /**
     * Конструктор по умолчанию.
     */
    public Catalog() {
    }

    /**
     * Получить список СД.
     *
     * @return список СД
     */
    public List<CD> getCdList() {
        return cdList;
    }
}
