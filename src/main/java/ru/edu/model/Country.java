package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JacksonXmlRootElement(localName = "Country")
public class Country implements Serializable {

    /**
     * Название страны.
     */
    @JacksonXmlProperty(localName = "name", isAttribute = true)
    @JsonProperty("name")
    private String name;

    /**
     * Список артистов из данной страны.
     */
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "Artist")
    @JsonProperty("artists")
    private List<Artist> artists = new ArrayList<>();

    /**
     * Конструктор с названием страны.
     *
     * @param pName
     */
    public Country(final String pName) {
        this.name = pName;
    }

    /**
     * Конструктор по умолчанию.
     */
    public Country() {
    }

    /**
     * Получение названия страны.
     *
     * @return имя
     */
    public String getName() {
        return name;
    }

    /**
     * Получение списка артистов страны.
     *
     * @return список артистов
     */
    public List<Artist> getArtists() {
        return artists;
    }

    /**
     * Добавление артиста в страну.
     *
     * @param artist Artist object
     */
    public void addArtist(final Artist artist) {
        artists.add(artist);
    }
}
