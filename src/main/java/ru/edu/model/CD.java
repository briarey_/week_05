package ru.edu.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "CD")
public class CD {

    /**
     * Оглавление.
     */
    @JacksonXmlProperty(localName = "TITLE")
    private String title;

    /**
     * Артист.
     */
    @JacksonXmlProperty(localName = "ARTIST")
    private String artist;

    /**
     * Страна.
     */
    @JacksonXmlProperty(localName = "COUNTRY")
    private String country;

    /**
     * Компания.
     */
    @JacksonXmlProperty(localName = "COMPANY")
    private String company;

    /**
     * Цена.
     */
    @JacksonXmlProperty(localName = "PRICE")
    private double price;

    /**
     * Год.
     */
    @JacksonXmlProperty(localName = "YEAR")
    private int year;

    /**
     * Конструктор с параметрами.
     *
     * @param pTitle
     * @param pArtist
     * @param pCountry
     * @param pCompany
     * @param pPrice
     * @param pYear
     */
    public CD(final String pTitle,
              final String pArtist,
              final String pCountry,
              final String pCompany,
              final double pPrice,
              final int pYear) {
        this.title = pTitle;
        this.artist = pArtist;
        this.country = pCountry;
        this.company = pCompany;
        this.price = pPrice;
        this.year = pYear;
    }

    /**
     * Конструктор по умолчанию.
     */
    public CD() {
    }

    /**
     * Получить оглавление.
     *
     * @return оглавление
     */
    public String getTitle() {
        return title;
    }

    /**
     * Получить артиста.
     *
     * @return артист
     */
    public String getArtist() {
        return artist;
    }

    /**
     * Получить страну.
     *
     * @return страна
     */
    public String getCountry() {
        return country;
    }

    /**
     * Получить компанию.
     *
     * @return компания
     */
    public String getCompany() {
        return company;
    }

    /**
     * Получить цену.
     *
     * @return цена
     */
    public double getPrice() {
        return price;
    }

    /**
     * Получить год.
     *
     * @return год
     */
    public int getYear() {
        return year;
    }
}
