package ru.edu;

import ru.edu.model.Registry;

public class RegistryExternalStorageJSON implements RegistryExternalStorage {

    /**
     * Реестр.
     */
    private Registry registry = new Registry();

    /**
     * Получить реестр.
     *
     * @return реестр
     */
    public Registry getRegistry() {
        return registry;
    }

    /**
     * Чтение из файла.
     *
     * @param filePath путь до файла
     */
    @Override
    public void readFrom(final String filePath) throws Exception {
        registry = MapperUtils.readJson(filePath, Registry.class);
    }

    /**
     * Запись реестра в файл.
     *
     * @param filePath путь
     * @param pRegistry реестр
     */
    @Override
    public void writeTo(final String filePath,
                        final Registry pRegistry) throws Exception {
        MapperUtils.mapJson(filePath, pRegistry);
    }
}
